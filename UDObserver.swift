//
//  UDObserver.swift
//
//  Created by SIARHEI LUKYANAU on 03.10.2022.
//

import Foundation
public protocol UDObserverProtocol {
    func udReceivedNotification(_ key: String)
}

public class UDObserver: SFSMonitorDelegate {

    let kAppExtGroupName = "group.com.securitycode.application.vpncontinent4"
    let monitorDispatchQueue =  DispatchQueue(label: "monitorDispatchQueue", qos: .utility)
    let fileManager = FileManager.default

    public var delegate: UDObserverProtocol?
    var queue: SFSMonitor?
    var userDefaults: UserDefaults?
    var groupPath: String?

    public init?(delegate: UDObserverProtocol? = nil) {
        self.delegate = delegate
        setObserver()
    }

// MARK: - ObserverKey

    open func addObserverKey(_ key: String) {
        let filePath = (groupPath ?? "") + key
        let answer = queue?.addURL(URL(fileURLWithPath: filePath))
        NSLog("addObserverKey answer: \(answer ?? 0)")
    }

    open func removeObserverKey(_ key: String) {
        let filePath = (groupPath ?? "") + key
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch {
            print("Не удалось удалить файл: \(error)")
        }
        _ = queue?.removeURL(URL(fileURLWithPath: filePath))
    }
    
    open func removeAllObserverKeys() {
        for watchedUrl in SFSMonitor.watchedURLs {
            do {
                try fileManager.removeItem(atPath: watchedUrl.key.path)
            } catch {
                print("Не удалось удалить файл: \(error)")
            }
        }
        _ = queue?.removeAllURLs()
    }
    
    open func isObserverKeyWatched(_ key: String) -> Bool {
        let filePath = (groupPath ?? "") + key
        return queue?.isURLWatched(URL(fileURLWithPath: filePath)) ?? false
    }
    
// MARK: - remove
    
    open func removeObject(forKey key: String) {
        userDefaults?.removeObject(forKey: key)
    }

    open func dictionaryRepresentation() -> [String : Any] {
        return userDefaults?.dictionaryRepresentation() ?? [String : Any]()
    }
    
// MARK: - set
        
    open func set(_ value: Any?, forKey key: String) {
        userDefaults?.set(value, forKey: key)
        userDefaults?.synchronize()
        sendNotification(key)
    }
    
    open func set(_ value: Int, forKey key: String) {
        userDefaults?.set(value, forKey: key)
        userDefaults?.synchronize()
        sendNotification(key)
    }

    open func set(_ value: Float, forKey key: String) {
        userDefaults?.set(value, forKey: key)
        sendNotification(key)
    }

    open func set(_ value: Double, forKey key: String) {
        userDefaults?.set(value, forKey: key)
        userDefaults?.synchronize()
        sendNotification(key)
    }

    open func set(_ value: Bool, forKey key: String) {
        userDefaults?.set(value, forKey: key)
        userDefaults?.synchronize()
        sendNotification(key)
    }

    open func set(_ url: URL?, forKey key: String) {
        userDefaults?.set(url, forKey: key)
        userDefaults?.synchronize()
        sendNotification(key)
    }

    // MARK: - get
        
    open func object(forKey key: String) -> Any? {
        return userDefaults?.object(forKey: key)
    }
    
    open func string(forKey key: String) -> String? {
        return userDefaults?.string(forKey: key)
    }

    open func array(forKey key: String) -> [Any]? {
        return userDefaults?.array(forKey: key)
    }

    open func dictionary(forKey key: String) -> [String : Any]? {
        return userDefaults?.dictionary(forKey: key)
    }

    open func data(forKey key: String) -> Data? {
        return userDefaults?.data(forKey: key)
    }

    open func stringArray(forKey key: String) -> [String]? {
        return userDefaults?.stringArray(forKey: key)
    }

    open func integer(forKey key: String) -> Int {
        return userDefaults?.integer(forKey: key) ?? 0
    }

    open func float(forKey key: String) -> Float {
        return userDefaults?.float(forKey: key) ?? 0
    }

    open func double(forKey key: String) -> Double {
        return userDefaults?.double(forKey: key) ?? 0
    }

    open func bool(forKey key: String) -> Bool {
        return userDefaults?.bool(forKey: key) ?? false
    }

    open func url(forKey key: String) -> URL? {
        return userDefaults?.url(forKey: key)
    }

    private func setObserver() {
        if let groupURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: kAppExtGroupName) {
            
            queue = SFSMonitor(delegate: self)
            queue?.setMaxMonitored(number: 200)

            userDefaults = UserDefaults.init(suiteName: kAppExtGroupName)
            groupPath = (groupURL.path) + "/"
        }
    }
    
    private func getUrlFromKey(_ key: String) -> URL {
        let filePath = (groupPath ?? "") + key
        let url = URL(fileURLWithPath: filePath)
        return url
    }
    
    private func sendNotification(_ key: String) {
        let url = getUrlFromKey(key)
        let data = Data()
        do {
            try data.write(to: url)
        } catch {
            print("Не удалось записать файл.")
        }
    }
    
    public func receivedNotification(_ notification: SFSMonitorNotification, url: URL, queue: SFSMonitor) {
        monitorDispatchQueue.async(flags: .barrier) {
            let key = url.lastPathComponent
            self.delegate?.udReceivedNotification(key)
        }
    }
}
